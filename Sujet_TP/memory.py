import numpy as np
import random

"""
@author vcoindet
Jeu de memory
"""

Tabl = ['a', 'a', 'b', 'b', 'c', 'c', 'd', 'd', 'e', 'e', 'f', 'f']

def melange_carte(Tab):
    '''

    Parameters
    ----------
    Tab : TYPE list
        DESCRIPTION liste de cartes

    Returns
    -------
    Tab : TYPE list
        DESCRIPTION liste de cartes où on a changé l'ordre des cartes dans la liste d'origine

    '''
    for i in range(1, len(Tab)):
        # pick an element in Tab[:i+1] with which to exchange Tab[i]
        j = int(random.random() * (i+1))
        Tab[i], Tab[j] = Tab[j], Tab[i]
    return Tab


def carte_cache(Tab):   #TODO
    '''

    Parameters
    ----------
    Tab : TYPE list
        DESCRIPTION liste de cartes

    Returns
    -------
    Tab : TYPE list
        DESCRIPTION Liste de cartes de même taille que Tab composée uniquement de 0
    '''
    long = len(Tab)
    lst = []
    for i in range (long):
        lst.append(0)
    return lst


def choisir_cartes(Tab):
    '''

    Parameters
    ----------
    Tab : TYPE list
        DESCRIPTION liste de cartes

    Returns
    -------
    list str, str
        DESCRIPTION liste de deux cartes distinctes parmi Tab

    '''
    c1 = int(input("Choisissez une carte : "))-1
    print(len(Tab))
    while (c1 >= len(Tab) or c1 < 0):
        c1 = int(input("Valeur hors de la liste. Veuillez choisir une autre carte : "))-1
    print(Tab[c1])
    c2 = int(input("Choisissez une deuxieme carte : "))-1
    while (c2 >= len(Tab) or c2 < 0 or c1 == c2):
        c2 = int(input("Valeur impossible. Veuillez choisir une autre carte : "))-1

    print(Tab[c2])
    return [c1,c2]


def retourne_carte (c1, c2, Tab, Tab_cache):    #TODO
    """
        TODO
        doit retrouner les cartes dans la liste cachée
    """
    new_tab = []
    for i in range(len(Tab)):         # on parcourt la table Tab
        if i != c1 and i != c2 :
            new_tab.append(Tab_cache[i])
        elif i == c1:
            new_tab.append(Tab[i])
        elif i == c2:
            new_tab.append(Tab[i])
    return new_tab

    
    

def jouer(Tab):
    '''
    

    Parameters
    ----------
    Tab : TYPE list
        DESCRIPTION liste de cartes

    Returns
    -------
    None.

    '''
    print("Bienvenue sur notre Jeu Memory, ci dessous voici les cartes mise en jeu. Il y en a 12")
    print(Tab)
    Tab = melange_carte(Tab)
    print("Maintenant je vais melanger les cartes et les retourner")
    
    Tab_cache =  carte_cache(Tab)
    print(Tab_cache)

    [c1, c2] = choisir_cartes(Tab)
    Tab_cache = retourne_carte(c1, c2, Tab, Tab_cache)
    print(Tab_cache)
    
    while 0 in Tab_cache:
        [c1, c2] = choisir_cartes(Tab)
        
        Tab_cache = retourne_carte(c1, c2, Tab, Tab_cache)
        
        print(Tab_cache)
        
    print("Bravo tu as gagné!!!")




if __name__ == '__main__':

    #test unitaire
    
    print(retourne_carte(0,1,Tabl, Tabcache1)==( ['a', 'a', 0,0,0,0,0,0,0,0,0,0]))
    
    ## test choisir carte: success
    
    
         
    print(carte_cache(Tabl)==([0]*12))

    tab_mel = melange_carte(Tabl)
    print(tab_mel)
    tab_cache = carte_cache(tab_mel)
    print(carte_cache)
        
        
        
    